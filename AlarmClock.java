import java.time.LocalTime;
import java.util.Scanner;
import java.time.format.DateTimeFormatter;
import java.time.DateTimeException;

class AlarmClock{
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Hello please set your Alarm in 24:00 Notation: ");

        String timeStamp = scan.nextLine();

        try{
            LocalTime time = LocalTime.parse(timeStamp, DateTimeFormatter.ISO_TIME);
            int Hour = time.getHour();
            int minute = time.getMinute();
            if(minute < 45){
                minute = 60 - (45 - minute);
                Hour = Hour - 1;
                if(Hour < 0){
                    Hour = 23;
                }
            }else{
                minute = minute - 45;
            }
            LocalTime newTime = LocalTime.of(Hour, minute);
            System.out.println("Congratulations your Alarm has been set for: " + newTime);

        }catch(DateTimeException exception){
            System.out.println(exception.getMessage());
        }

        scan.close();
    }
}